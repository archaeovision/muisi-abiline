// Saves options to chrome.storage
function save_options() {
  let printers = [];
  let label1 = document.getElementById("label1").value;
  let printer1 = document.getElementById("printer1").value
    ? document.getElementById("printer1").value
    : "";
  let label2 = document.getElementById("label2").value
    ? document.getElementById("label2").value
    : "";
  let printer2 = document.getElementById("printer2").value;
  if (printer1) {
    // lets store only if value is present
    printers.push({ key: 1, printer: printer1, template: label1 });
  }
  if (printer2) {
    // lets store only if value is present
    printers.push({ key: 2, printer: printer2, template: label2 });
  }

  chrome.storage.sync.set(
    {
      printers: printers,
    },
    function () {
      // Update status to let user know options were saved.
      var status = document.getElementById("status");
      status.textContent = "Seaded salvestatud.";
      setTimeout(function () {
        status.textContent = "";
      }, 750);
    }
  );
}

// Clears options from chrome.storage
function clear_options1() {
  document.getElementById("label1").value = "";
  document.getElementById("printer1").value = "";
}
function clear_options2() {
  document.getElementById("label2").value = "";
  document.getElementById("printer2").value = "";
}
function restore_options() {
  chrome.storage.sync.get(
    {
      printers: "",
    },
    function (items) {
      document.getElementById("label1").value = items.printers[0].template
        ? items.printers[0].template
        : "";
      document.getElementById("printer1").value = items.printers[0].printer
        ? items.printers[0].printer
        : "";
      document.getElementById("label2").value = items.printers[1].template
        ? items.printers[1].template
        : "";
      document.getElementById("printer2").value = items.printers[1].printer
        ? items.printers[1].printer
        : "";
    }
  );
}
document.addEventListener("DOMContentLoaded", restore_options);
document.getElementById("save").addEventListener("click", save_options);
document.getElementById("clear1").addEventListener("click", clear_options1);
document.getElementById("clear2").addEventListener("click", clear_options2);
