(function ($) {
  /**
   * Start application when page is loaded.
   */

  initializeOnPageLoad();

  /**
   * Start application when button is clicked.
   */

  $(document).on("DOMSubtreeModified", "#sylemForm #sylemWR", function (e) {
    let exportLink = $(e.target, "#museaalid fieldset legend");
    if (exportLink.length) {
      initializeOnButtonClick(e);
    } else return false;
  });

  function initializeOnPageLoad() {
    // Check if page is 'SylemiHaldus'
    if ("SylemiHaldus" === findGetParameter("page")) {
      loadData();
      createButtons();
      getDataFromTable();
    }
  }

  function initializeOnButtonClick(e) {
    if (
      e.target.lastElementChild != null &&
      e.target.lastElementChild.id != null
    ) {
      window.location.reload(false);
      if (e.target.lastElementChild.id === "museaalid") {
        loadData();
        createButtons();
        getDataFromTable();
      }
    }
  }

  function loadData() {
    // Writing printer settings to local storage from Chrome extension storage
    chrome.storage.sync.get("printers").then((result) => {
      localStorage.removeItem("printers");
      localStorage.setItem("printers", JSON.stringify(result.printers));
    });
  }

  function createButtons() {
    if ($("#museaalid fieldset legend").length) {
      let printButton = "";
      // Loading printer settings from local storage
      const printers =
        localStorage.getItem("printers") != "undefined"
          ? JSON.parse(localStorage.getItem("printers"))
          : null;
      // Button for each printer
      if (printers) {
        printers.forEach(function (printer) {
          printButton +=
            "<a href='#' id='printer" +
            printer.key +
            "'><span class='img-container' title='Trüki sildid printerisse " +
            printer.key +
            "'><img src='" +
            chrome.runtime.getURL("img/print-solid.svg") +
            "' alt='Trüki sildid'><span class='img-text'>" +
            printer.key +
            "</span></span></a>";
        });
      }

      $("#museaalid fieldset legend").append(
        "<span id='ma-buttons'><strong>MuISi abiline</strong> <a href='#' id='exportMuisID'><img src='" +
          chrome.runtime.getURL("img/file-csv-solid.svg") +
          "' alt='CSV fail koos MuISi ID-ga' title='CSV fail koos MuISi ID-ga'></a> " +
          printButton +
          " </span>"
      );
    }
  }

  function getDataFromTable() {
    //check if there are contents in table
    var sylemRow = $("#sylemResultTableWR tbody tr");
    if (sylemRow.length) {
      var output = new Array();
      var printerOutput = new Array();
      // table header
      var header = $(document).find("#sylemResultTableWR thead th");
      var number_label = header[1].textContent.replace(/^\s+|\s+$/g, "");
      var name_label = header[3].textContent.replace(/^\s+|\s+$/g, "");
      var muis_id_label = "MuIS ID";
      // "\uFEFF" makes Excel to open file n UTF8 mode
      output.push(
        "\uFEFF" + number_label + ";" + name_label + ";" + muis_id_label
      );
      printerOutput.push(
        "\uFEFF" + number_label + "||" + name_label + "||" + muis_id_label
      );

      //var output = new Array();
      for (var i = 0; i < sylemRow.length; i++) {
        var cell = $("td", sylemRow[i]);
        var number = cell[1].textContent.replace(/^\s+|\s+$/g, "");
        var name = cell[3].textContent.replace(/^\s+|\s+$/g, "");
        var muis_id = $("a[href]", cell[7])[0]["href"].replace(
          /\D*&sp=.(\d*)&sp=[\w\W]*/g,
          "$1"
        );
        output.push(number + ";'" + name + "';" + muis_id);
        printerOutput.push(number + '||' + name + '||' + muis_id);
      }
      /**
       * output CSV when export-link is clicked
       */
      $("#museaalid fieldset legend")
        .find("#exportMuisID")
        .click(function () {
          initDownload(output.join("\n"));
        });
      // Loading printer settings from local storage
      const printers =
        localStorage.getItem("printers") != "undefined"
          ? JSON.parse(localStorage.getItem("printers"))
          : null;

      /**
       * Prints labels when clicked
       */
      if (printers) {
        printers.forEach(function (printer) {
          $("#museaalid fieldset legend")
            .find("#printer" + printer.key)
            .click(function () {
              printZPL(printerOutput, printer.key, 1);
            });
        });
      }
    }
  }

  /**
   * Find parameters from page
   * @param {*} parameterName
   */
  function findGetParameter(parameterName) {
    var result = null,
      tmp = [];
    location.search
      .substr(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
      });
    return result;
  }

  function initDownload(content) {
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(
      new Blob([content], { type: "text/csv" })
    );
    a.download = "muisist.csv";

    // Append anchor to body.
    document.body.appendChild(a);
    a.click();

    // Remove anchor from body
    document.body.removeChild(a);
  }
})(jQuery);
