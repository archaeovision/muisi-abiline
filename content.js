(function ($) {
  // Define all the rdf elements and html ids.
  let fields = {
    "muis:acr": ["aAcr", "searchAcr", "acr"],
    "muis:trt": ["atrt", "trt"],
    "muis:trs": ["atrs", "trs"],
    "muis:trj": ["atrj", "trj"],
    "muis:trl": ["atrl", "trl"],
    "muis:kt": ["akt", "kt"],
    "muis:ks": ["aks", "ks"],
    "muis:kl": ["akl", "kl"],
    "muis:kj": ["akj", "kj"],
  };

  // Define html form ids and submit ids.
  let formIds = {
    objektiKompleksparingForm: ["ajaxSubmit"],
    ooKopeerimine: ["ajaxSubmit"],
    AjaxForm: ["ajaxSubmit"],
    searchAcr: ["submit"],
    searchForm: ["submitSearch"],
    objektiOtsingForm: ["ajaxSubmit", "oodTulemusteSalvestaAS1"],
    uodcForm: ["uodcTulemusteOtsiAS"],
    form: ["teostaF"],
    kohakataloogForm: ["ajaxSubmit"],
  };
  // Load form when one of those exists.
  let requiredFields = fields["muis:acr"];

  /** Find page types to exlude from url get args.
   * https://www.muis.ee/muis/app?page=InventuuriLoomine&service=page page=InventuuriLoomine
   */
  let pagesToExlude = ["InventuuriLoomine"];
  // Init active form;
  let activeForm;
  // Start app.
  initialize();
  // subscribe to DOMSubtreeModified so we can work with ajax modals.
  $(document).bind("DOMSubtreeModified", function () {
    if ($(".dojoDialog form").length > 0) {
      setTimeout(function () {
        initialize();
        $("#museaaliId").trigger("focus");
      }, 700);
    }
  });
  
  /**
   * Start application.
   */
  function initialize() {
    let exclude = false;
    // Exit if page type is one of those.
    $.each(pagesToExlude, function (index) {
      if (pagesToExlude[index] === findGetParameter("page")) {
        exclude = true;
      }
    });
    // Make sure theres need for adding museal ID field.
    //console.log($('input[id^="Acr"]'));
    if ($("#museaaliId").length != 0 || exclude) {
      return;
    }
    let $form;
    $.each(formIds, function (formId) {
      let $forms = $('form[id^="' + formId + '"]');
      let inputExists = false;
      // loop through all the forms using this id pattern.
      if ($forms.length > 0) {
        $.each($forms, function (index) {
          $form = $(this);
          $.each(requiredFields, function () {
            let input = $form.find('input[name="' + this + '"]');
            if (input.length > 0) {
              inputExists = true;
            }
          });
          if ($form.length > 0) {
            if (!inputExists) {
              return;
            }
            activeForm = $form.attr("id");
            let museal_number_field =
              '<div id="musealId-wrapper"><div><strong>MuISi abiline</strong></div><div><label>Museaali ID:</label> <input type="text" id="museaaliId"></div></div>';
            $form.prepend(museal_number_field);
            $("#museaaliId").trigger("focus");
            inputExists = false;
          }
        });
      }
    });

    let musnr = "";
    // React on paste and keyup events.
    $("#museaaliId").bind("paste, keyup", function (e) {
      setTimeout(function () {
        let musnrnow = $("#museaaliId").val();
        if (musnr != musnrnow) {
          loadMuisData(musnrnow);
          musnr = musnrnow;
          $("#museaaliId").val("");
        }
      }, 500);
    });
    // Init after pushing reset button. https://www.muis.ee/search
    $("#" + activeForm)
      .find('input[name="cancel"]')
      .click(function () {
        setTimeout(function () {
          initialize();
        }, 300);
      });
  }
  /**
   * Load Muis data.
   * @param {String} id
   */
  function loadMuisData(id) {
    if (id.length > 0) {
      let url = location.origin + "/rdf/object/" + id;

      $.get(url, function (response, status) {
        if (status == "success") {
          if ($("#museaaliNr").length > 0) {
            updateMusealNr(response);
            submitForm();
          } else {
            updateNumbrivahemik(response);
            submitForm();
          }
        } else {
          console.log("failed to load muis data. Network error");
        }
      });
    }
  }
  function submitForm() {
    // submit form
    $.each(formIds[activeForm], function (index) {
      // console.log($('#' + activeForm).find('input[id^="'+ this +'"]'));
      $("#" + activeForm)
        .find('input[id^="' + this + '"]')
        .trigger("click");
    });
  }
  /**
   *
   * @param {Object} response
   */
  function updateMusealNr(response) {
    let museaalinr = loadFromXML("dcterms:identifier", response);
    $("#museaaliNr").val(museaalinr);
    $("#museaaliNrCb").attr({ checked: true });
  }
  /**
   *
   * @param {Object} response;
   */
  function updateNumbrivahemik(response) {
    let info = dataFromElements(response, fields);
    // Loop through the fields and find the information for the field.
    for (let field in fields) {
      let fieldIds = fields[field];
      for (let fieldId in fieldIds) {
        let $htmlid = $('input[id^="' + fields[field][fieldId] + '"]');
        if ($htmlid.length > 0) {
          $htmlid.val(info[field]);
        }
      }
    }
  }
  /**
   * Read data from fields
   * @param {XMLDocument} xml
   * @param {String} tags
   */
  function dataFromElements(xml, tags) {
    let itemsOut = [];
    // Load data fro each rdf element
    for (let tag in tags) {
      var data = loadFromXML(tag, xml);
      if (data) {
        itemsOut[tag] = data;
      }
    }
    return itemsOut;
  }
  /**
   * Load data from xml object
   * @param {XMLDocument} xml
   * @param {String} tags
   */
  function loadFromXML(tag, xml) {
    if (xml.getElementsByTagName(tag).length > 0) {
      return xml.getElementsByTagName(tag)[0].innerHTML;
    }
    return false;
  }
  /**
   * Find parameters from page
   * @param {*} parameterName
   */
  function findGetParameter(parameterName) {
    var result = null,
      tmp = [];
    location.search
      .substr(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
      });
    return result;
  }
  /** Disable enter key for forms at this website otherwise with will create problems with barcode reader */
  $(document).ready(function () {
    $(window).keydown(function (event) {
      if (event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
  });
})(jQuery);
