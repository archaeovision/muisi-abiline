# MuIS abiline #

## Formid kus MuIS ID otsing kasutusel ##
* I etapis kirjeldamata museaalid *id=AjaxForm*
* II etapis kirjeldamata museaalid *id=AjaxForm*
* Objektide otsing *id=objektiOtsingForm*
* Objekti sisukirje päring *id=objektiKompleksparingForm*
* Näitusele objektide lisamine omast muuseumist *id=uodcForm*

## Nupud mille klikkimist vaja kontrollida ##
* *id=openObjektiOtsingDialogAS*
* *id=lisaMuseaal*
* *id=nupp*

## Inventuur ##
* Inventuuri tulemused *id=form*

## Printimine
Sülemi lehel on võimalik printida silte klõpsates printeri-ikoonil. Sõltuvalt mitu printerit on konfigureeritud, nii mitu nupukest kuvatakse.
Printimine on võimalik võrguprinteritesse. USB kaudu seadistatud printerit hetkel kasutada pole võimalik.

## Versioonid
### 1.5.2
* Vene- ja kreekakeelsed tähed ei toiminud (#2)
* ZPL 76x152mm riptutatava sildi jaoks
* ; põhjustas vea - eraldaja vahetatud || vastu

### 1.5.1
* CSV ikoon ei kuvatud kui printer polnud määratud
  
### 1.5
* Printeri seadistuse salvestamine lisamine laienduse seadististuse alla

### 1.4
* ZPL 40x50 riputatava sildi jaoks
* ZPL 50*25 kleepsu jaoks
* Kinnitusaken enne printimist, kus kirjas et mis printer ja mis suuruses silt ja et mitu silti prindib
* Mitme printeri tugi koos sildi suuruse konfigureerimise võimalusega
* Manifesti uuendamine versioonile 3
* Eraldiseiseb printeri konfiguratsiooni fail
  
### 1.3
* Printeri toe lisamine

## TODO
* Tee ringi getPrintTemplate nii et printeri ja sildi info päritakse ainult üks kord, mitte iga sildi loomisel