/**
 * Printing functionality for MuIS extension
 */

/**
 * Retrieves printing template
 * @param {number} template - Label template name.
 * @param {number} muisi_id
 * @param {string} museaali_number
 * @param {string} nimetus
 */
function getPrintTemplate(template, muisi_id, museaali_number, nimetus) {
  let getTemplateParams = templates().filter((t) => t.key == template);
  const templateParams = {};
  getTemplateParams.map(function (params) {
    templateParams["label_width"] = params.width;
    templateParams["label_height"] = params.height;
    templateParams["museaali_number_font"] = params.museaali_number_font;
    templateParams["nimetus_font"] = params.nimetus_font;
    return templateParams;
  });

  const fontParams = (field) =>
    templateParams[field].map(({ length, size }) => ({ length, size }));

  const getFontSize = (fontParams, field, value) => {
    for (const { length, size } of fontParams) {
      if (field.length < length) {
        return value === "length" ? length : size;
      }
    }
    return value === "length"
      ? fontParams[fontParams.length - 1].length
      : fontParams[fontParams.length - 1].size;
  };

  const museaali_number_size = getFontSize(
    fontParams("museaali_number_font"),
    museaali_number
  );
  const nimetus_size = getFontSize(fontParams("nimetus_font"), nimetus);
  const nimetus_length = getFontSize(
    fontParams("nimetus_font"),
    nimetus,
    "length"
  );

  // let's cut the 'nimetus' value to fit the page with
  nimetus =
    nimetus.length > nimetus_length
      ? nimetus.substring(0, nimetus_length - 3) + "..."
      : nimetus;
  const bar_width = template != 3 ? (muisi_id.length < 7 ? 4 : 3) : 5;
  const bar_code_position = Math.round(
    (templateParams["label_width"] -
      ((muisi_id.length + 2) * 11 + 13) * bar_width) /
      2
  );
  const fillTemplate = templates(
    templateParams["label_width"],
    templateParams["label_height"],
    bar_code_position,
    bar_width,
    muisi_id,
    museaali_number_size,
    museaali_number,
    nimetus_size,
    nimetus
  ).filter((t) => t.key == template);

  const zpl = fillTemplate.map(function (getZpl) {
    return getZpl.zpl;
  });
  //console.log(zpl[0]);
  return zpl[0];
}

/**
 * Sends labels to selected printer
 * @param {array} input - Values for printing as array.
 * @param {number} printer - Printer name (key).
 * @param {number} template - Label template name (key).
 */
function printZPL(input, printer) {
  chrome.storage.sync.get("printers").then((result) => {
    const output = [];
    input.forEach((row) => {
      const label_values = row.split("||");
      const museaali_number = label_values[0];
      //const nimetus = label_values[1].replace(/['"]+/g, "");
      const nimetus = label_values[1];
      const muisi_id = label_values[2];
      output.push(
        getPrintTemplate(
          result.printers[printer - 1].template,
          muisi_id,
          museaali_number,
          nimetus
        )
      );
    });

    output.shift(); // first, title row removed
    const label_count = output.length;

    let templateKey = result.printers[printer - 1].template;
    let getTemplateName = templates().filter((t) => t.key == templateKey);
    let templateName = getTemplateName.map(function (getName) {
      return getName.value;
    });

    if (
      confirm(
        "\nTrükin " +
          label_count +
          " silti.\n\n" +
          "Sildi suurus: " +
          templateName +
          "\n\n" +
          "Printer: " +
          printer +
          " - " +
          result.printers[printer - 1].printer +
          "\n\nKas jätkan?"
      ) == true
    ) {
      $.ajax({
        method: "POST",
        url: "http://" + result.printers[printer - 1].printer + ":9100/pstprnt",
        data: output.join("\n"),
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        timeout: 3000, // important - if timeout is not set ajax call is not executed again in case ov wanting to print same label again
        cache: false,
        context: this,
      });
      console.log(output.join("\n"));
    }
  });
}
