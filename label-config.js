/**
 * Label template config
 * Version: 1.1
 */
const templates = (
  label_width,
  label_height,
  bar_code_position,
  bar_width,
  muisi_id,
  museaali_number_size,
  museaali_number,
  nimetus_size,
  nimetus
) => [
  {
    key: 1,
    value: "40x50 mm riputatav",
    width: 460,
    height: 530,
    museaali_number_font: [
      { length: 16, size: "60,20" },
      { length: 50, size: "30,10" },
    ],
    nimetus_font: [
      { length: 90, size: "40,30" },
      { length: 200, size: "30,30" },
    ],    
    zpl: `^XA 
      ^PON^LH0,0^FWN
      ^CI28
      ^CWZ,E:TT0003M_.TTF 
      ^PW${label_width}
      ^LL${label_height}
      ^FT${bar_code_position},0,1^BY${bar_width},2,150^FWN,2^BC^FD${muisi_id}^FS
      ^CFD,${museaali_number_size}
      ^FO10,220
      ^FB430,1,0,C
      ^FD${museaali_number}^FS 
      ^A@N,${nimetus_size},TT0003M.TTF
      ^FO10,300
      ^FB435,6,10
      ^FD${nimetus}^FS 
      ^XZ`,
  },
  {
    key: 2,
    value: "50x25 mm kleebis",
    width: 560,
    height: 300,
    museaali_number_font: [
      { length: 25, size: "60,20" },
      { length: 50, size: "30,10" },
    ],
    nimetus_font: [
      { length: 70, size: "40,30" },
      { length: 110, size: "40,20" },
    ],
    zpl: `^XA
    ^PON^LH0,0^FWN
    ^CI28
    ^PW${label_width}
    ^LL${label_height}
    ^CFD,${museaali_number_size}
    ^FO0,10
    ^FB${label_width},1,0,C
    ^FD${museaali_number}^FS 
    ^FO0,70
    ^FB${label_width},2,10
    ^A@N,${nimetus_size},TT0003M.TTF
    ^FD${nimetus}^FS 
    ^FT${bar_code_position},250,1^BY${bar_width},2,90^FWN,2^BC^FD${muisi_id}^FS
    ^XZ`,
  },
  {
    key: 3,
    value: "76x152 mm riputatav",
    width: 800,
    height: 1600,
    museaali_number_font: [
      { length: 20, size: "90,30" },
      { length: 50, size: "70,20" },
    ],
    nimetus_font: [
      { length: 100, size: "100,60" },
      { length: 300, size: "70,50" },
    ],
    zpl: `^XA
    ^POI^LH0,0^FWN
    ^CI28
    ^PW${label_width}
    ^LL${label_height}
    ^CFD,${museaali_number_size}
    ^FO0,200
    ^FB${label_width},2,0,C
    ^FD${museaali_number}^FS 
    ^FO10,400
    ^FB${label_width-10},10,15
    ^A@N,${nimetus_size},TT0003M.TTF
    ^FD${nimetus}^FS 
    ^FT${bar_code_position},${label_height},1^BY${bar_width},2,300^FWN,2^BC^FD${muisi_id}^FS
    ^XZ`,
  },
];